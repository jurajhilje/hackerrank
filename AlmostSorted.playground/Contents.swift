import Foundation
import XCTest

func isSorted(arr: [Int]) -> Bool {
    if arr.count == 1 { return true }
    
    for i in 1..<arr.count {
        if arr[i - 1] > arr[i] { return false }
    }
    
    return true
}

func almostSorted(arr: [Int]) {
    var A = arr
    let N = arr.count
    
    if isSorted(arr: A) {
        print("yes")
        return
    }
    
    var left = 0
    var right = 0
    
    // find first not ordered item from left
    for i in 1..<N {
        if A[i - 1] > A[i] {
            left = i - 1
            break
        }
    }
    
    // find first not ordered item from right
    for i in 1..<N {
        let i = N - i
        if A[i - 1] > A[i] {
            right = i
            break
        }
    }
    
    if left == right {
        print("no")
        return
    }
    
    var swaps = 0
    var l = left
    var r = right
    
    while r - l > 0 {
        if A[l] > A[r] {
            A.swapAt(l, r)
            swaps += 1
        }
        r -= 1
        l += 1
    }
    
    if isSorted(arr: A) {
        print("yes")
        if swaps == 1 {
            print("swap \(left + 1) \(right + 1)")
        } else {
            print("reverse \(left + 1) \(right + 1)")
        }
    } else {
        print("no")
    }
    
}

// Function tests

almostSorted(arr: [4, 2])
almostSorted(arr: [3, 1, 2])
almostSorted(arr: [1, 5, 4, 3, 2, 6])

// Unit tests

class AlmostSortedTests: XCTestCase {
    
    func testIsSorted() {
        XCTAssertTrue(isSorted(arr: [1]))
        XCTAssertTrue(isSorted(arr: [1, 2, 3]))
        XCTAssertFalse(isSorted(arr: [3, 1, 2]))
    }

}

AlmostSortedTests.defaultTestSuite.run()
